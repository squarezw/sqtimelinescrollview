//
//  SQTimelineScrollViewController.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQTimelineScrollViewController.h"
#import "GGFullscreenImageViewController.h"
#import "objc/runtime.h"
#import "DRImagePlaceholderHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIView+WhenTappedBlocks.h"

#define DEFAULT_HEIGHT_OFFSET 52.0f

@implementation SQTimelineScrollViewController

- (id)init
{
    if (self = [super init]) {
        self.cells = [[NSMutableArray alloc] initWithCapacity:0];
        [self initialize];
    }
    return self;    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.cells = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;    
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    
    // build table view of time line
    self.timelineTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  
    self.timelineTableView.delegate = self;
    self.timelineTableView.dataSource = self;

    // header view
    self.timelineTableView.tableHeaderView = [self buildPullRefreshHeaderView:CGRectMake(0, 0, self.timelineTableView.frame.size.width, 160.0f)];

    // bg
    [self buildBackgroundView];
    
    [self.timelineTableView.tableHeaderView addSubview:[self buildUserProfileView:self.timelineTableView.tableHeaderView.bounds]];
    
    // footer view
    self.timelineTableView.tableFooterView = [self buildPullRefreshFooterView:CGRectMake(0, 0, self.timelineTableView.frame.size.width, 60.0f)];
    [self.timelineTableView.tableFooterView setClipsToBounds:YES];
    
    [self.view addSubview:self.timelineTableView];
    
    [self initData];
    
}

- (void)initData
{
    // abstract
}

- (UIImage*)backgroundHeaderView
{
    return [UIImage imageNamed:@"background"];
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
//    return 0;
    return [self.cells count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQCellModel* model = [self.cells objectAtIndex:indexPath.row];
    
    SQScrollCellViewMixImage* obj = [[SQScrollCellViewMixImage alloc] init];
    obj.cellObject = model;
    return [obj getContentHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"SQTimelineCell";
	SQScrollCellViewMixImage *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    SQCellModel* model = [self.cells objectAtIndex:indexPath.row];
    
	if (cell == nil) {
//		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell = [[SQScrollCellViewMixImage alloc] initWithCell:model clickedAvatarBlock:^(UIImageView *avatarView, NSError *error) {
            
            NSLog(@".sssss.");
            
        } clickedImageBlock:^(UIImageView *imageView, NSError *error) {
            
            GGFullscreenImageViewController *vc = [[GGFullscreenImageViewController alloc] init];
            vc.liftedImageView = imageView;
            [self presentViewController:vc animated:YES completion:nil];
            
        }];
        
        if (model.event) {
            [cell setEventIcon:[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",model.event] ]]];
        }
        
        [self addEventForImageInCell:cell.imageViews];
        
	}
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	return cell;
}

- (void)addEventForImageInCell:(NSArray*)imageViews
{
    for (SQScrollCellImageView* imageView in imageViews ) {
        
        UIButton* button = [[UIButton alloc] initWithFrame:imageView.bounds];
        [button addTarget:self action:@selector(clickedImageHandlerInCell:) forControlEvents:UIControlEventTouchUpInside];
        
        [imageView setUserInteractionEnabled:YES];
        [imageView addSubview:button];
        
        if ([imageView.cellImage.originalImageURL hasPrefix:@"http"]) {
            __weak SQScrollCellImageView* wImageView = imageView;
            
            [imageView setImageWithURL:[NSURL URLWithString:imageView.cellImage.imageURL] placeholderImage:[imageView defaultPlaceHolderImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                
                if (!image) {
                    [wImageView removeFromSuperview];
                }
                
            }];
        }
    }
}

- (void)clickedImageHandlerInCell:(UIButton*)button
{
    UIImageView* imageView = (UIImageView*)[button superview];
    if (imageView) {
        GGFullscreenImageViewController *vc = [[GGFullscreenImageViewController alloc] init];
        vc.liftedImageView = imageView;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)clickedAvatarHandlerInHeaderView:(SQUserModel *)userObject
{

}

- (SQPullRefreshHeaderView* ) buildPullRefreshHeaderView:(CGRect)frame
{
    _refreshHeaderView = [[SQPullRefreshHeaderView alloc] initWithFrame:frame];
    _refreshHeaderView.delegate = self;
    
    return _refreshHeaderView;
}

- (SQPullRefreshFooterView*) buildPullRefreshFooterView:(CGRect)frame
{
    _refreshFooterView = [[SQPullRefreshFooterView alloc] initWithFrame:frame];
    _refreshFooterView.delegate = self;
    
    return _refreshFooterView;
}

- (void)buildBackgroundView
{
    // bg
    UIImage* imageBg = [self backgroundHeaderView];
    
    if (imageBg) {
        
        UIImageView* imageView = [[UIImageView alloc] initWithImage:imageBg];
        [imageView setFrame:CGRectMake(0, self.timelineTableView.contentOffset.y - (imageBg.size.height - self.timelineTableView.tableHeaderView.frame.size.height), imageBg.size.width, imageView.frame.size.height)];
        
        [self.timelineTableView.tableHeaderView insertSubview:imageView atIndex:0];
        
    }
}

- (UIView*)buildUserProfileView:(CGRect)frame
{
    if (!_userProfileView) {
        _userProfileView = [[SQScrollHeaderView alloc] initWithFrame:frame];
        [_userProfileView setBackgroundColor:[UIColor clearColor]];
    }
    return _userProfileView;
}

- (void)setCurrentUser:(SQUserModel *)currentUser
{
    if (currentUser) {
        _currentUser = currentUser;
        
        __weak SQTimelineScrollViewController* wself = self;
        
        self.userProfileView.clickedAvatarHandler = ^(SQUserModel *userObject) {
            
            SQTimelineScrollViewController* sself = wself;
            
            if (sself) {
                [sself clickedAvatarHandlerInHeaderView:userObject];
            }
            
        };
        self.userProfileView.userObject = _currentUser;
    }
}

#pragma mark -

- (void)reloadDataSource:(SQPullRefreshDataSource)source_{
	
    _reloading = YES;
    
    [self realodData:source_];
    
}

- (void)doneLoadingData:(SQPullRefreshDataSource)source_ attachData:(NSArray*)attachData_{

    _reloading = NO;
    
    if (attachData_ && [attachData_ count] > 0) {
        
        if (source_ == SQPullRefreshDataOfNew) {
            
            [self.cells removeAllObjects];
            
            self.cells = [[NSMutableArray alloc] initWithArray:attachData_];
            
            [self.timelineTableView reloadData];
            
        } else if (source_ == SQPullRefreshDataOfPast) {
            
            [self.cells addObjectsFromArray:attachData_];
            
            [self.timelineTableView reloadData];
        }
        
    }
    
    if (source_ == SQPullRefreshDataOfNew) {
        
        [self refreshCompleted];
        
        [_refreshHeaderView stopLoading];
        
    } else if (source_ == SQPullRefreshDataOfPast) {
        
        [self loadMoreCompleted];
        
        [_refreshFooterView stopLoading];
        
    }
    
//    if (source_ == SQPullRefreshDataOfPast && _refreshFooterView) {
//        [_refreshFooterView removeFromSuperview];
//        _refreshFooterView = nil;
//        
//        [self buildPullRefreshFooterView:self.scrollView];
//    }
    
//    if ([_refreshHeaderView respondsToSelector:@selector(resetTopViewInScrollView)]) {
//        [_refreshHeaderView performSelector:@selector(resetTopViewInScrollView)];
//    }
//    
//	[_refreshHeaderView refreshScrollViewDataSourceDidFinishedLoading:self.timelineTableView];
	
}

- (void)realodData:(SQPullRefreshDataSource)source_
{
    if (source_ == SQPullRefreshDataOfNew) {
        [_refreshHeaderView startLoading];
//        SQCellModel* model = [[SQCellModel alloc] initWithAvatar:nil nickname:@"new boy" date:[NSDate date] content:@"test test test" images:@[@"http://cdn.iciba.com/web/co/logo/logo_index.gif"] videoPath:nil vote:1];
//        [self.cells insertObject:model atIndex:0];
        
    } else if (source_ == SQPullRefreshDataOfPast) {
        [_refreshFooterView startLoading];
//        NSMutableArray* cellsMore = [[NSMutableArray alloc] init];
//        SQCellModel* model = [[SQCellModel alloc] initWithAvatar:nil nickname:@"new boy" date:[NSDate date] content:@"test test test" images:@[@"defaultAvatar"] videoPath:nil vote:1];
//        [cellsMore addObject:model];
        
    }
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    
//}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    if (_refreshHeaderView)
//        [_refreshHeaderView refreshScrollViewDidScroll:scrollView];
//    
//    if (_refreshFooterView)
//        [_refreshFooterView refreshScrollViewDidScroll:scrollView];
//    
//}

//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//	
//    if (_refreshHeaderView)
//        [_refreshHeaderView refreshScrollViewDidEndDragging:scrollView];
//    
//    if (_refreshFooterView)
//        [_refreshFooterView refreshScrollViewDidEndDragging:scrollView];
//	
//}

#pragma mark -
#pragma mark RefreshHeaderDelegate Methods

- (void)refreshDidTriggerRefresh:(SQPullRefreshView*)view{
	
    if ([view isKindOfClass:[SQPullRefreshHeaderView class]]) {
        [self reloadDataSource:SQPullRefreshDataOfNew];
        
    } else if ([view isKindOfClass:[SQPullRefreshFooterView class]]) {
        
        [self reloadDataSource:SQPullRefreshDataOfPast];
        
    }
	
}

- (BOOL)refreshDataSourceIsLoading:(SQPullRefreshView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

#pragma mark - pull refresh & load more



- (void) initialize
{
    _pullToRefreshEnabled = YES;
    
    _canLoadMore = YES;
    
    _clearsSelectionOnViewWillAppear = YES;
}

#pragma mark - Pull to Refresh

////////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat) headerRefreshHeight
{
    if (!CGRectIsEmpty(headerViewFrame))
        return headerViewFrame.size.height;
    else
        return DEFAULT_HEIGHT_OFFSET;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) pinHeaderView
{
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.timelineTableView.contentInset = UIEdgeInsetsMake([self headerRefreshHeight], 0, 0, 0);
    }];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) unpinHeaderView
{
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.timelineTableView.contentInset = UIEdgeInsetsZero;
    }];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) willBeginRefresh
{
    if (_pullToRefreshEnabled)
        [self pinHeaderView];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) willShowHeaderView:(UIScrollView *)scrollView
{
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) headerViewDidScroll:(BOOL)willRefreshOnRelease scrollView:(UIScrollView *)scrollView
{
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) refresh
{
    if (_isRefreshing)
        return NO;
    
    [self willBeginRefresh];
    _isRefreshing = YES;
    
    [self realodData:SQPullRefreshDataOfNew];
    
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) refreshCompleted
{
    _isRefreshing = NO;
    
    if (_pullToRefreshEnabled)
        [self unpinHeaderView];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Load More

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) willBeginLoadingMore
{
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) loadMoreCompleted
{
    _isLoadingMore = NO;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) loadMore
{
    if (_isLoadingMore)
        return NO;
    
    [self willBeginLoadingMore];
    _isLoadingMore = YES;
    
    [self realodData:SQPullRefreshDataOfPast];
    
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat) footerLoadMoreHeight
{
    if (_refreshFooterView)
        return _refreshFooterView.frame.size.height;
    else
        return DEFAULT_HEIGHT_OFFSET;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) setFooterViewVisibility:(BOOL)visible
{
    if (visible && self.timelineTableView.tableFooterView != _refreshFooterView)
        self.timelineTableView.tableFooterView = _refreshFooterView;
    else if (!visible)
        self.timelineTableView.tableFooterView = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) allLoadingCompleted
{
    if (_isRefreshing)
        [self refreshCompleted];
    if (_isLoadingMore)
        [self loadMoreCompleted];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIScrollViewDelegate

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (_isRefreshing)
        return;
    _isDragging = YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"%f", [self footerLoadMoreHeight]);
//    NSLog(@"%f ---- %f, %f, %f", scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y, scrollView.contentSize.height, scrollView.frame.size.height, scrollView.contentOffset.y);
    
    if (!_isRefreshing && _isDragging && scrollView.contentOffset.y < 0) {
        [self headerViewDidScroll:scrollView.contentOffset.y < 0 - [self headerRefreshHeight]
                       scrollView:scrollView];
    } else if (!_isLoadingMore && _canLoadMore && scrollView.contentOffset.y > 100) {
        CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
        if (scrollPosition < [self footerLoadMoreHeight]) {
            [self loadMore];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (_isRefreshing)
        return;
    
    _isDragging = NO;
    if (scrollView.contentOffset.y <= 0 - [self headerRefreshHeight]) {
        if (_pullToRefreshEnabled)
            [self refresh];
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) releaseViewComponents
{
    self.timelineTableView = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) dealloc
{
    [self releaseViewComponents];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) viewDidUnload
{
    [self releaseViewComponents];
    [super viewDidUnload];
}

@end
