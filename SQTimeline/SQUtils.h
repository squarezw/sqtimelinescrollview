//
//  SQUtils.h
//  sqtest
//
//  Created by apple on 13-9-12.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NRSimplePlist.h"

#define SQRGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define SQLOGRECT(rect) \
NSLog(@"%s x=%f, y=%f, w=%f, h=%f", #rect, rect.origin.x, rect.origin.y, \
rect.size.width, rect.size.height)

#define SQLOGPOINT(pt) \
NSLog(@"%s x=%f, y=%f", #pt, pt.x, pt.y)

#define SQLOGSIZE(size) \
NSLog(@"%s w=%f, h=%f", #size, size.width, size.height)

@interface SQUtils : NSObject

+ (id) getValueFromSettingPlist:(NSString*)key;

@end
