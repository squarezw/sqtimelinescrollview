//
//  SQScrollCellImageView.m
//  SQTimeline
//
//  Created by apple on 13-10-8.
//  Copyright (c) 2013年 square. All rights reserved.
//

#import "SQScrollCellImageView.h"
#import "DRImagePlaceholderHelper.h"

@implementation SQScrollCellImageView

- (void)dealloc
{
    
}

- (UIImage*)defaultPlaceHolderImage
{
    return [[DRImagePlaceholderHelper sharedInstance] placerholderImageWithSize:self.frame.size text:NSLocalizedString(@"loading", nil)];
}

@end
