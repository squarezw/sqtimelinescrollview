//
//  SQHeaderView.h
//  sqtest
//
//  Created by apple on 13-9-12.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQAvatarView.h"
#import "SQUserModel.h"

typedef void(^SQClickedAvatarInHeaderBlock)(SQUserModel *userObject);

@interface SQScrollHeaderView : UIView

@property (nonatomic, retain) SQUserModel* userObject;
@property (nonatomic, retain) UIView* topView;
@property (nonatomic, retain) UILabel* nicknameLabel;
@property (nonatomic, retain) UILabel* summaryLabel;
@property (nonatomic, retain) UIImageView * avatarWrapper;
@property (nonatomic, retain) SQAvatarViewCircle* avatarView;
@property (nonatomic, retain) UIView* backgroundView;
@property (nonatomic, copy) SQClickedAvatarInHeaderBlock clickedAvatarHandler;
@property (nonatomic, assign) CGRect initTopViewFrame;

- (id)initWithFrame:(CGRect)frame;

- (void)holdTopViewInScrollView:(UIScrollView*)scrollView lastContentOffset:(NSValue*)lastContentOffset;
- (void)resetTopViewInScrollView;

@end



