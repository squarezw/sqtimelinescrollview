//
//  SQScrollCellView.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//
#import "SQScrollCellView.h"
#import <QuartzCore/QuartzCore.h>
#import "NSDate+TimeAgo.h"

@implementation SQCellBackgroundView

- (void)dealloc
{
    
}

- (id)initWithFrame:(CGRect)frame LeftSpace:(CGFloat)leftSpace_
{
    if (self = [super initWithFrame:frame]) {
        self.leftSpace = leftSpace_;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
	if ( context )
    {
        CGFloat leftSpace = self.leftSpace;
        
        // point background
        CGContextSetFillColorWithColor(context,
                                       SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"CellBackground.color-red"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"CellBackground.color-green"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"CellBackground.color-blue"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"CellBackground.color-alpha"] floatValue]).CGColor
                                       );
        CGContextFillRect(context, CGRectMake(0, 0, self.frame.size.width, self.frame.size.height));
        CGContextFillPath( context );
        
        // point vertical line
        CGContextBeginPath(context);
        CGContextSetLineWidth(context, [[SQUtils getValueFromSettingPlist:@"verticalLine.weight"] floatValue]);
        CGContextSetStrokeColorWithColor(context,
                                         SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"verticalLine.color-red"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-green"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-blue"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-alpha"] floatValue]).CGColor
                                         );
        CGContextMoveToPoint(context, leftSpace + 17.5f, 0.0);
        CGContextAddLineToPoint(context, leftSpace + 17.5f, self.frame.size.height);
        CGContextStrokePath(context);
        
        // point middle circle
        CGContextAddEllipseInRect(context, CGRectMake(leftSpace + 11.5f, 22, 12, 12));
        CGContextSetFillColorWithColor(context,
                                       SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"middleCircle.color-red"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"middleCircle.color-green"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"middleCircle.color-blue"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"middleCircle.color-alpha"] floatValue]).CGColor
                                       );
        CGContextFillPath( context );
        
        // point small circle in middle circle
        CGContextAddEllipseInRect(context, CGRectMake(leftSpace + 13.5f, 24, 8, 8));
        CGContextSetFillColorWithColor(context,
                                       SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"smallCircleInMiddelCircle.color-red"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"smallCircleInMiddelCircle.color-green"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"smallCircleInMiddelCircle.color-blue"] floatValue],
                                                   [[SQUtils getValueFromSettingPlist:@"smallCircleInMiddelCircle.color-alpha"] floatValue]).CGColor
                                       );
        CGContextFillPath( context );
        
//        // point split line
//        CGContextBeginPath(context);
//        CGContextSetLineWidth(context, [[SQUtils getValueFromSettingPlist:@"splitLine.height"] floatValue]);
//        CGContextSetStrokeColorWithColor(context,
//                                         SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"splitLine.color-red"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"splitLine.color-green"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"splitLine.color-blue"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"splitLine.color-alpha"] floatValue]).CGColor
//                                         );
//        CGContextMoveToPoint(context, 0, self.frame.size.height);
//        CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
//        CGContextStrokePath(context);
        
    }
}

@end

@implementation SQScrollCellView

- (id)initWithCell:(SQCellBaseModel *)cell_
{
    if (self = [super init]) {
        self.cellObject = cell_;
        
        [self initUI];

    }
    return self;
}

- (void)initUI
{
    SQCellBackgroundView* bgView = [[SQCellBackgroundView alloc] initWithFrame:self.frame LeftSpace:[self getLeftSpace]];
    [bgView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundView:bgView];
    
    if (_cellObject) {
        NSLog(@"...");
        
        [self buildAvatarView];
        
        CGFloat leftSpace = [self getLeftSpace];
        CGFloat rightSpace = [self getRightSpace];
        
        self.nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftSpace + 35, 5, 160, 30)];
        [self.nicknameLabel setBackgroundColor:[UIColor clearColor]];
        [self.nicknameLabel setTextColor:SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"nicknameLabel.color-red"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"nicknameLabel.color-green"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"nicknameLabel.color-blue"] floatValue],
                                                     [[SQUtils getValueFromSettingPlist:@"nicknameLabel.color-alpha"] floatValue])];
        [self.nicknameLabel setText:self.cellObject.nickname];
        [self.nicknameLabel setFont:[UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"nicknameLabel.font-size"] floatValue]]];
        
        [self addSubview:self.nicknameLabel];
        
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 80, 5, 75, 30)];
        [self.dateLabel setBackgroundColor:[UIColor clearColor]];
        self.dateLabel.textAlignment = UITextAlignmentRight;
        [self.dateLabel setTextColor:SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"dateLabel.color-red"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"dateLabel.color-green"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"dateLabel.color-blue"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"dateLabel.color-alpha"] floatValue])];
        [self.dateLabel setFont:[UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"dateLabel.font-size"] floatValue]]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd HH:mm"];
        NSString *strDate = [dateFormatter stringFromDate:self.cellObject.date];
//        [self.dateLabel setText:[self.cellObject.date timeAgo]];
        [self.dateLabel setText:strDate];
        [self addSubview:self.dateLabel];
        
        self.mixContent = [[UIView alloc] initWithFrame:CGRectMake(leftSpace + 35, 35, rightSpace, 20)];
        self.mixContent.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self buildContentView];
        [self addSubview:self.mixContent];
        
    }
    
}

- (CGFloat)getLeftSpace
{
    CGFloat leftSpace = 0;
    
    if (self.hideAvatar == NO) {
        leftSpace = [[SQUtils getValueFromSettingPlist:@"leftSpace.width"] floatValue];
    } else {
        leftSpace = [[SQUtils getValueFromSettingPlist:@"leftSpace.widthWhenHideAvatar"] floatValue];
    }
    return leftSpace;
}

- (CGFloat)getRightSpace
{
    CGFloat rightSpace = 0;

    if (self.hideAvatar == NO) {
        rightSpace = [[SQUtils getValueFromSettingPlist:@"rightSpace.width"] floatValue];
    } else {
        rightSpace = [[SQUtils getValueFromSettingPlist:@"rightSpace.widthWhenHideAvatar"] floatValue];
    }
    return rightSpace;
}

- (void)buildContentView
{
    UILabel* contentLabel = [[UILabel alloc] initWithFrame:self.mixContent.bounds];
    contentLabel.backgroundColor = [UIColor clearColor];
    contentLabel.text = [self.cellObject.content stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    contentLabel.numberOfLines = 0;
    contentLabel.textColor = SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"contentLabel.color-red"] floatValue],
                                         [[SQUtils getValueFromSettingPlist:@"contentLabel.color-green"] floatValue],
                                         [[SQUtils getValueFromSettingPlist:@"contentLabel.color-blue"] floatValue],
                                         [[SQUtils getValueFromSettingPlist:@"contentLabel.color-alpha"] floatValue]);
    contentLabel.font = [UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"contentLabel.font-size"] floatValue]];
    [contentLabel sizeToFit];
    
    [self.mixContent addSubview:contentLabel];
}

- (void)buildAvatarView
{
    if (self.hideAvatar == YES) {
        return;
    }
    
    
    if (!_avatarView) {
        self.avatarView = [[SQAvatarView alloc] initWithFrame:CGRectMake(10, 10, 35, 35)];
    }
    
    [self.avatarView setAvatarImage:self.cellObject.avatar];
    
    self.avatarView.layer.cornerRadius = 3;
    self.avatarView.clipsToBounds = YES;
    
    [self addSubview:self.avatarView];
}

- (float)getContentHeight
{
    // default height;
    CGSize contentSize;
    contentSize = [[self.cellObject.content stringByReplacingOccurrencesOfString:@"\n" withString:@" "]
                   sizeWithFont:[UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"contentLabel.font-size"] floatValue]]
                   constrainedToSize:CGSizeMake([self getRightSpace], MAXFLOAT)
                   lineBreakMode:UILineBreakModeWordWrap];
    
    return MAX(20.0, contentSize.height) + 5;
    
//    float contentHeight = 0.0;
//    
//    if (self.mixContent.subviews) {
//        
//        for (UIView* v in self.mixContent.subviews) {
//            
//            contentHeight += v.frame.size.height;
//            
//        }
//        
//    } else {
//        
//        contentHeight = self.mixContent.frame.size.height;
//        
//    }
//    
//    contentHeight += 5; // spacing
//
//    return contentHeight;
}

@end
