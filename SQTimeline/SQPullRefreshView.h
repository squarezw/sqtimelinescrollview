//
//  SQPullRefreshHeaderView.h
//  sqtest
//
//  Created by apple on 13-9-9.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum
{
    PullRefreshNormal = 0,
    PullRefreshPulling,
    PullRefreshLoading,
} PullRefreshState;

@protocol PullRefreshDelegate;

@interface SQPullRefreshView : UIView
{
    PullRefreshState _state;
}

@property (nonatomic, retain) UIActivityIndicatorView *activityView;

@property (nonatomic, weak) id <PullRefreshDelegate> delegate;


- (void) initself:(CGRect)frame;

- (void) startLoading;
- (void) stopLoading;

// in trigger region
- (BOOL) inTriggerRefresh:(UIScrollView *)scrollView;

// out trigger region
- (BOOL) outTriggerRefresh:(UIScrollView *)scrollView;

- (void) autoOffsetScrollInset:(UIScrollView *)scrollView;

- (void) refreshScrollViewDidScroll:(UIScrollView *)scrollView;
- (void) refreshScrollViewDidEndDragging:(UIScrollView *)scrollView;
- (void) refreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView;

@end

@interface SQPullRefreshHeaderView : SQPullRefreshView


@end

@interface SQPullRefreshFooterView : SQPullRefreshView

@end

@protocol PullRefreshDelegate <NSObject>
- (void) refreshDidTriggerRefresh:(SQPullRefreshView *)view;
- (BOOL) refreshDataSourceIsLoading:(SQPullRefreshView *)view;

@end
