//
//  SQCellBaseModel.h
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SQCellBaseModel : NSObject

@property (nonatomic, retain) NSString* avatar;
@property (nonatomic, retain) NSString* nickname;
@property (nonatomic, retain) NSDate* date;
@property (nonatomic, retain) NSString* content;

- (id)initWithAvatar:(NSString*)avatar_
            nickname:(NSString*)nickname_
                date:(NSDate*)date_
             content:(NSString*)content_;

@end

@interface SQCellModel : SQCellBaseModel

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, retain) NSArray* images; // SQCellImage
@property (nonatomic, retain) NSString* videoPath;
@property (nonatomic, assign) NSInteger vote;
@property (nonatomic, retain) NSString* event;

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString*)avatar_ nickname:(NSString*)nickname_ date:(NSDate*)date_ content:(NSString*)content_ images:(NSArray*)images_;

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString*)avatar_ nickname:(NSString*)nickname_ date:(NSDate*)date_ content:(NSString*)content_ images:(NSArray*)images videoPath:(NSString*)videoPath_ vote:(NSInteger)vote_;

@end
