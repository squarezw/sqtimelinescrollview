//
//  SQCellImage.h
//  SQTimeline
//
//  Created by apple on 13-10-22.
//  Copyright (c) 2013年 square. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQCellImage : NSObject

@property (nonatomic, retain) NSString* imageURL;
@property (nonatomic, retain) NSString* originalImageURL;
@property (nonatomic, assign) NSInteger placeHolderWeight;
@property (nonatomic, assign) NSInteger placeHolderHeight;

@end
