//
//  SQCellBaseModel.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQCellModel.h"

@implementation SQCellBaseModel

- (id)initWithAvatar:(NSString*)avatar_
            nickname:(NSString*)nickname_
                date:(NSDate*)date_
             content:(NSString*)content_
{
    if (self = [super init]) {
        self.avatar = avatar_;
        self.nickname = nickname_;
        self.date = date_;
        self.content = content_;
    }
    return self;
}

@end

@implementation SQCellModel

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString*)avatar_ nickname:(NSString*)nickname_ date:(NSDate*)date_ content:(NSString*)content_ images:(NSArray *)images_
{
    if ([self initWithIndex:index_ avatar:avatar_ nickname:nickname_ date:date_ content:content_ images:images_ videoPath:nil vote:NSNotFound]) {
        
    }
    return self;
}

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString*)avatar_ nickname:(NSString*)nickname_ date:(NSDate*)date_ content:(NSString*)content_ images:(NSArray *)images_ videoPath:(NSString *)videoPath_ vote:(NSInteger)vote_
{
    if (self = [super init]) {
        self.index = index_;
        self.avatar = avatar_;
        self.nickname = nickname_;
        self.date = date_;
        self.content = content_;
        self.images = images_;
        self.videoPath = videoPath_;
        self.vote = vote_;
    }
    return self;
}

@end