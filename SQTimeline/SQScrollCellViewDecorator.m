//
//  SQScrollCellViewDecorator.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQScrollCellViewDecorator.h"
#import "UIView+WhenTappedBlocks.h"
#import "SQCellImage.h"
#import "DRImagePlaceholderHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation SQScrollCellViewDecorator

- (id)initWithCell:(SQCellBaseModel *)cell_ clickedAvatarBlock:(SQClickedAvatarBlock)block
{
    if (self = [super initWithCell:cell_]) {
        self.clickedAvatarHandler = block;
    }
    return self;
}

- (void)buildContentView
{
    [super buildContentView];
    
    __weak SQScrollCellViewDecorator* wself = self;
    
    [self.avatarView whenTouchedUp:^{
        
        SQScrollCellViewDecorator *sself = wself;
        if (sself) {
            sself.clickedAvatarHandler(sself.avatarView, nil);
        }
    }];
}

- (void)setEventIcon:(UIView *)eventIcon
{
    if (_eventIcon) {
        [_eventIcon removeFromSuperview];
        _eventIcon = nil;
    }
    _eventIcon = eventIcon;
    [_eventIcon setFrame:CGRectMake(50.5f, 16, 24, 24)];
    [self addSubview:_eventIcon];
}

/*
- (UIImage*)defaultAvatarImage
{
    return [UIImage imageNamed:@"defaultAvatar"];
}
*/

@end


////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation SQScrollCellViewMixImage

- (id)initWithCell:(SQCellBaseModel *)cell_ clickedAvatarBlock:(SQClickedAvatarBlock)avatarBlock clickedImageBlock:(SQClickedImageBlock)imageblock
{
    if (self = [super initWithCell:cell_ clickedAvatarBlock:avatarBlock]) {
        self.clickedImageHandler = imageblock;
    }
    return self;
}

- (void)buildContentView
{
    [super buildContentView];
    [self buildImagesView:self.mixContent images:self.cellObject.images];
}

- (void)buildImagesView:(UIView*)view_ images:(NSArray*)images
{
    float contentHeight = [super getContentHeight];
    CGRect frame = view_.bounds;
    frame.origin.y += contentHeight;
        
    __weak SQScrollCellViewMixImage* wself = self;
    
    if (!_imageViews) {
        _imageViews = [[NSMutableArray alloc] init];
    }

    for (SQCellImage* image in images) {

        SQScrollCellImageView* imageView = [[SQScrollCellImageView alloc] init];

        if ([image.originalImageURL hasPrefix:@"http"]) {
            
            imageView.cellImage = image;
            imageView.frame = CGRectMake(frame.origin.x, frame.origin.y, image.placeHolderWeight, image.placeHolderHeight);
            
        } else {
            imageView.image = [UIImage imageNamed:image.imageURL];
            imageView.frame = CGRectMake(frame.origin.x, frame.origin.y, imageView.image.size.width, imageView.image.size.height);
        }
        
        imageView.backgroundColor = [UIColor whiteColor];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.clipsToBounds = YES;
        
        [view_ addSubview:imageView];
        
        [self.imageViews addObject:imageView];
        
        frame.origin.y = frame.origin.y + imageView.frame.size.height;
    }
    
}

- (float)getContentHeight
{
    float height = [super getContentHeight];
    float imageHeight = 0;
    
    for (SQCellImage* image in self.cellObject.images) {
        
        imageHeight += image.placeHolderHeight;
        
    }
    
    height += imageHeight;
    
    return height;
}

@end