//
//  SQPullRefreshHeaderView.m
//  sqtest
//
//  Created by apple on 13-9-9.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQPullRefreshView.h"

#define TEXT_COLOR	 [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0]
#define FLIP_ANIMATION_DURATION 0.18f

@interface SQPullRefreshView (Private)
- (void)setState:(PullRefreshState)aState;
@end

@implementation SQPullRefreshView

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
		
        [self initself:frame];
		
    }
	
    return self;
	
}

- (void) initself:(CGRect)frame
{
    self.activityView =[[UIActivityIndicatorView alloc]  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.activityView setCenter:CGPointMake(frame.size.width*0.5, 20)];
//    [self.activityView startAnimating];
    [self addSubview:self.activityView];
    
}

- (void)startLoading
{
    [self.activityView startAnimating];
}

- (void)stopLoading
{
    [self.activityView stopAnimating];
}

#pragma mark -
#pragma mark Setters

- (void)setState:(PullRefreshState)aState{
	
	switch (aState) {
		case PullRefreshPulling:
			
            [_activityView startAnimating];
            
			break;
		case PullRefreshNormal:        
			
			[_activityView stopAnimating];
			
			break;
		case PullRefreshLoading:
			
			[_activityView startAnimating];
			
			break;
            
		default:
			break;
	}
	
	_state = aState;
}


#pragma mark -
#pragma mark ScrollView Methods

- (void)refreshScrollViewDidScroll:(UIScrollView *)scrollView {
	
	if (_state == PullRefreshLoading) {
		
		[self autoOffsetScrollInset:scrollView];
		
	} else if (scrollView.isDragging) {
        
        if (_state == PullRefreshNormal && [self inTriggerRefresh:scrollView] && ![self isLoadingDataSource]) {
            
            [self setState:PullRefreshPulling];
            
        } else if (_state == PullRefreshPulling && [self outTriggerRefresh:scrollView] && ![self isLoadingDataSource]){
            
            [self setState:PullRefreshNormal];
            
        }
        
//      if (scrollView.contentInset.top != 0) {
//			scrollView.contentInset = UIEdgeInsetsZero;
//		}
		
	}
	
}

- (void)refreshScrollViewDidEndDragging:(UIScrollView *)scrollView {
    
	if ([self inTriggerRefresh:scrollView] && ![self isLoadingDataSource]) {
		
        [self setState:PullRefreshLoading];
        
		if ([_delegate respondsToSelector:@selector(refreshDidTriggerRefresh:)]) {
			[_delegate refreshDidTriggerRefresh:self];
		}
        
        [self autoOffsetScrollInset:scrollView];
		        
    }
	
}

- (void)autoOffsetScrollInset:(UIScrollView *)scrollView
{
    // abstract
}

- (BOOL)inTriggerRefresh:(UIScrollView *)scrollView
{
    // abstract
    return NO;
}

- (BOOL)outTriggerRefresh:(UIScrollView *)scrollView
{
    // abstract
    return ![self inTriggerRefresh:scrollView];
}

- (BOOL)isLoadingDataSource
{
    BOOL _loading = NO;
    if ([_delegate respondsToSelector:@selector(refreshDataSourceIsLoading:)]) {
        _loading = [_delegate refreshDataSourceIsLoading:self];
    }
    
    return _loading;
}

- (void)refreshScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView {
    NSLog(@"scrollView: %@", scrollView);
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        [scrollView setContentInset:UIEdgeInsetsZero];
    }];
    
    NSLog(@"scrollView: %@", scrollView);
    
	[self setState:PullRefreshNormal];
}


@end

@implementation SQPullRefreshHeaderView

- (BOOL)inTriggerRefresh:(UIScrollView *)scrollView
{
    return scrollView.contentOffset.y < -65.0f;
}

- (BOOL)outTriggerRefresh:(UIScrollView *)scrollView
{
    return scrollView.contentOffset.y > -65.0f && scrollView.contentOffset.y < 0.0f;
}

- (void)autoOffsetScrollInset:(UIScrollView *)scrollView
{
    CGFloat offset = MAX(scrollView.contentOffset.y * -1, 0);
    offset = MIN(offset, 60);
    [UIView animateWithDuration:0.3 animations:^(void) {
        scrollView.contentInset = UIEdgeInsetsMake(offset, 0.0f, 0.0f, 0.0f);
    }];
}

@end


@implementation SQPullRefreshFooterView

- (BOOL)inTriggerRefresh:(UIScrollView *)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffsetY = scrollView.contentOffset.y;
    
    return (scrollOffsetY + scrollViewHeight -  scrollContentSizeHeight) > 60.0f;
}

- (BOOL)outTriggerRefresh:(UIScrollView *)scrollView
{
    return ![self inTriggerRefresh:scrollView];
}

- (void)autoOffsetScrollInset:(UIScrollView *)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    
    CGFloat offset = scrollContentSizeHeight + scrollViewHeight + 60;
    offset = MIN(offset, 60);
    [UIView animateWithDuration:0.3 animations:^(void) {
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, offset, 0.0f);
    }];
}

@end