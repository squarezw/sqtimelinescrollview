//
//  SQUserModel.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQUserModel.h"
#import "SQUtils.h"

@implementation SQUserModel

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString *)avatar_ nickname:(NSString *)nickname_ summary:(NSString *)summary_
{
    if (self = [super init]) {
        self.index = index_;
        self.avatar = avatar_;
        self.nickname = nickname_;
        self.summary = summary_;
    }
    return self;
}

- (NSString*)avatar
{
    if (!_avatar) {
        return [SQUtils getValueFromSettingPlist:@"avatarView.avatarDefaultImageName"];
    }
    return _avatar;
}

@end
