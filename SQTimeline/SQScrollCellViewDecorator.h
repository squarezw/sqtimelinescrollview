//
//  SQScrollCellViewDecorator.h
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQScrollCellView.h"
#import "SQCellModel.h"
#import "SQScrollCellImageView.h"

typedef void(^SQClickedAvatarBlock)(UIImageView *avatarView, NSError *error);

@interface SQScrollCellViewDecorator : SQScrollCellView

@property (nonatomic, retain) UIView* eventIcon;
@property (nonatomic, retain) SQCellModel* cellObject;
@property (nonatomic, copy) SQClickedAvatarBlock clickedAvatarHandler;

- (id)initWithCell:(SQCellBaseModel *)cell_ clickedAvatarBlock:(SQClickedAvatarBlock)block;

@end


typedef void(^SQClickedImageBlock)(UIImageView *imageView, NSError *error);

@interface SQScrollCellViewMixImage : SQScrollCellViewDecorator

@property (nonatomic, copy) SQClickedImageBlock clickedImageHandler;
@property (nonatomic, retain) NSMutableArray* imageViews; // SQScrollCellImageView

- (id)initWithCell:(SQCellBaseModel *)cell_ clickedAvatarBlock:(SQClickedAvatarBlock)avatarBlock clickedImageBlock:(SQClickedImageBlock)imageblock;

@end