//
//  SQUtils.m
//  sqtest
//
//  Created by apple on 13-9-12.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQUtils.h"

#ifndef kSQScrollSettingsFilename
#define kSQScrollSettingsFilename @"SQScrollSettings"
#endif

@implementation SQUtils

+ (id) getValueFromSettingPlist:(NSString*)key
{
    if ([key rangeOfString:@"."].location == NSNotFound) {
        
        return [NRSimplePlist valuePlist:kSQScrollSettingsFilename withKey:key];
        
    } else {
        
        NSArray* keys = [key componentsSeparatedByString:@"."];
        
        NSDictionary* dict = [NRSimplePlist valuePlist:kSQScrollSettingsFilename withKey:[keys objectAtIndex:0]];
        
        if (dict) {
            return dict[keys.lastObject];
        } else {
            return nil;
        }
    }
}

@end
