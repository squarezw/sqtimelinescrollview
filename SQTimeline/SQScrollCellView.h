//
//  SQScrollCellView.h
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQCellModel.h"
#import "SQAvatarView.h"
#import "SQUtils.h"

@interface SQCellBackgroundView : UIView

@property (nonatomic, assign) CGFloat leftSpace;

@end

@interface SQScrollCellView : UITableViewCell

@property (nonatomic, retain) SQAvatarView* avatarView;
@property (nonatomic, retain) UILabel* nicknameLabel;
@property (nonatomic, retain) UILabel* dateLabel;
@property (nonatomic, retain) UIView* mixContent;
@property (nonatomic, retain) SQCellBaseModel* cellObject;
@property (nonatomic, assign) BOOL hideAvatar;

- (id)initWithCell:(SQCellBaseModel*)cell_;
- (void)initUI;

- (void)buildContentView;
- (void)buildAvatarView;

/*! Get height of content in cell view

 * \returns height of content
 
 */
- (float)getContentHeight;

@end
