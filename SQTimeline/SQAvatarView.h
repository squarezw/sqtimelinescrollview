//
//  SQAvatarView.h
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQAvatarView : UIImageView

@property (nonatomic, retain) UIImage* defaultAvatarImage;

- (void)setAvatarImage:(NSString *)imageName;

@end


@interface SQAvatarViewCircle : UIView

@property (strong, nonatomic) UIImage *image;

@end
