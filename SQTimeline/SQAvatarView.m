//
//  SQAvatarView.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQAvatarView.h"
#import <UIImageView+WebCache.h>
#import "DRImagePlaceholderHelper.h"
#import "SQUtils.h"
#import <QuartzCore/QuartzCore.h>

@implementation SQAvatarView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)setAvatarImage:(NSString *)imageName
{
    if (imageName) {
        if ([imageName hasPrefix:@"http"]) {
            
            [self setImageWithURL:[NSURL URLWithString:imageName] placeholderImage:[self defaultAvatarImage]];
            
        } else {
            
            [self setImage:[UIImage imageNamed:imageName]];
            
        }
    } else {
        
        [self setImage:[self defaultAvatarImage]];
        
    }
    
}

- (UIImage*)defaultAvatarImage
{
    if (!_defaultAvatarImage) {
        return [[DRImagePlaceholderHelper sharedInstance] placerholderAvatarWithSize:self.frame.size];
    }
    return _defaultAvatarImage;
}

@end

@implementation SQAvatarViewCircle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    /*
    CGRect b = self.bounds;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(ctx);
    
    CGPathRef circlePath = CGPathCreateWithEllipseInRect(b, 0);
    CGMutablePathRef inverseCirclePath = CGPathCreateMutableCopy(circlePath);
    CGPathAddRect(inverseCirclePath, nil, CGRectInfinite);
    
    CGContextSaveGState(ctx); {
        CGContextBeginPath(ctx);
        CGContextAddPath(ctx, circlePath);
        CGContextClip(ctx);
        [_image drawInRect:b];
    } CGContextRestoreGState(ctx);
    
    CGContextSaveGState(ctx); {
        CGContextBeginPath(ctx);
        CGContextAddPath(ctx, circlePath);
        CGContextClip(ctx);
        
        CGContextSetShadowWithColor(ctx, CGSizeMake(0, 0), 3.0f, [UIColor colorWithRed:0.994 green:0.989 blue:1.000 alpha:1.0f].CGColor);
        
        CGContextBeginPath(ctx);
        CGContextAddPath(ctx, inverseCirclePath);
        CGContextEOFillPath(ctx);
    } CGContextRestoreGState(ctx);
    
    CGPathRelease(circlePath);
    CGPathRelease(inverseCirclePath);
    
    CGContextRestoreGState(ctx);
    */
    
    [_image drawInRect:self.bounds];
}

@end
