//
//  SQUserModel.h
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQUserModel : NSObject

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, retain) NSString* avatar;
@property (nonatomic, retain) NSString* nickname;
@property (nonatomic, retain) NSString* summary;

- (id)initWithIndex:(NSInteger)index_ avatar:(NSString*)avatar_ nickname:(NSString*)nickname_ summary:(NSString*)summary_;

@end
