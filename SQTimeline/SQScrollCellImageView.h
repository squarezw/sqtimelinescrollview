//
//  SQScrollCellImageView.h
//  SQTimeline
//
//  Created by apple on 13-10-8.
//  Copyright (c) 2013年 square. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "SQCellImage.h"

@interface SQScrollCellImageView : UIImageView

@property (nonatomic, retain) SQCellImage* cellImage;

@property (nonatomic, retain) UIImage* defaultPlaceHolderImage;

@end
