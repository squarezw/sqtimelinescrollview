//
//  SQHeaderView.m
//  sqtest
//
//  Created by apple on 13-9-12.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "SQScrollHeaderView.h"
#import "SQUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIView+WhenTappedBlocks.h"

@implementation SQScrollHeaderView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
	if ( context )
    {
        // point vertical line
//        CGContextBeginPath(context);
//        CGContextSetLineWidth(context, [[SQUtils getValueFromSettingPlist:@"verticalLine.weight"] floatValue]);
//        CGContextSetStrokeColorWithColor(context,
//                                         SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"verticalLine.color-red"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-green"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-blue"] floatValue],
//                                                     [[SQUtils getValueFromSettingPlist:@"verticalLine.color-alpha"] floatValue]).CGColor
//                                         );
//        CGContextMoveToPoint(context, 65, self.frame.size.height - 100);
//        CGContextAddLineToPoint(context, 65, self.frame.size.height);
//        CGContextStrokePath(context);
    }
}

- (void)initUI
{
    if (CGRectIsEmpty(self.initTopViewFrame)) {
        self.initTopViewFrame = self.bounds;
    }
    
    self.topView = [[UIView alloc] initWithFrame:self.initTopViewFrame];
    
    [self.topView setBackgroundColor:[UIColor clearColor]];
    
    self.avatarView = [[SQAvatarViewCircle alloc] initWithFrame:CGRectMake(40, self.frame.size.height - 70, 45, 45)];
    self.avatarView.image = [UIImage imageNamed:[SQUtils getValueFromSettingPlist:@"avatarView.avatarDefaultImageName"]];
    self.avatarView.layer.cornerRadius = 22.5f;
    self.avatarView.clipsToBounds = YES;
    
    [self.topView addSubview:self.avatarView];

    
    // avatar Wrapper
    UIImage* image = [UIImage imageNamed:[SQUtils getValueFromSettingPlist:@"avatarView.avatarWrapperImageName"]];
    self.avatarWrapper = [[UIImageView alloc] initWithFrame:CGRectMake(38, self.frame.size.height - 72, _avatarView.frame.size.width + 4, image.size.height)];
    self.avatarWrapper.image = image;
    [self.topView insertSubview:self.avatarWrapper belowSubview:_avatarView];


    self.nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, self.frame.size.height - 70, 205, 25)];
    [self.nicknameLabel setTextColor:SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"nicknameLabelInHeader.color-red"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"nicknameLabelInHeader.color-green"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"nicknameLabelInHeader.color-blue"] floatValue],
                                                 [[SQUtils getValueFromSettingPlist:@"nicknameLabelInHeader.color-alpha"] floatValue])];
    [self.nicknameLabel setBackgroundColor:[UIColor clearColor]];
    [self.nicknameLabel setFont:[UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"nicknameLabelInHeader.font-size"] floatValue]]];
    
    [self.topView addSubview:self.nicknameLabel];
        

    
    self.summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, self.frame.size.height - 45, 205, 25)];
    [self.summaryLabel setTextColor:SQRGBACOLOR([[SQUtils getValueFromSettingPlist:@"summaryLabelInHeader.color-red"] floatValue],
                                                [[SQUtils getValueFromSettingPlist:@"summaryLabelInHeader.color-green"] floatValue],
                                                [[SQUtils getValueFromSettingPlist:@"summaryLabelInHeader.color-blue"] floatValue],
                                                [[SQUtils getValueFromSettingPlist:@"summaryLabelInHeader.color-alpha"] floatValue])];
    [self.summaryLabel setBackgroundColor:[UIColor clearColor]];
    [self.summaryLabel setFont:[UIFont systemFontOfSize:[[SQUtils getValueFromSettingPlist:@"summaryLabelInHeader.font-size"] floatValue]]];
    
    [self.topView addSubview:self.summaryLabel];
    
    [self addSubview:self.topView];
}


- (void)setBackgroundView:(UIView *)backgroundView
{
    if (_backgroundView) {
        [_backgroundView removeFromSuperview];
        _backgroundView = nil;
    }
    _backgroundView = backgroundView;
    [_backgroundView setContentMode:UIViewContentModeScaleAspectFill];
    [_backgroundView setFrame:self.bounds];
    [self addSubview:_backgroundView];
    [self sendSubviewToBack:_backgroundView];
}

- (void)setUserObject:(SQUserModel *)userObject
{
    _userObject = userObject;
    
    if (_userObject.avatar) {
        if ([_userObject.avatar hasPrefix:@"http"]) {
            SDWebImageDownloader* downloader = [SDWebImageDownloader sharedDownloader];
            [downloader downloadImageWithURL:[NSURL URLWithString:_userObject.avatar] options:SDWebImageDownloaderLowPriority progress:^(NSUInteger receivedSize, long long expectedSize) {
                
            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                
                if (image) {
                    if (_avatarView) {
                        _avatarView.image = image;
                        [_avatarView setNeedsDisplay];
                    }
                }
                
            }];
        } else {
            _avatarView.image = [UIImage imageNamed:_userObject.avatar];
            [_avatarView setNeedsDisplay];
        }
    }
    
    
    
    self.nicknameLabel.text = _userObject.nickname;
    
    self.summaryLabel.text = _userObject.summary;
    
}

- (void)setClickedAvatarHandler:(SQClickedAvatarInHeaderBlock)clickedAvatarHandler
{
    _clickedAvatarHandler = clickedAvatarHandler;
    
    if (clickedAvatarHandler) {
        __weak SQScrollHeaderView* wself = self;
        
        [_avatarView whenTouchedUp:^{
            
            SQScrollHeaderView *sself = wself;
            if (sself) {
                sself.clickedAvatarHandler(sself.userObject);
            }
        }];
    }

}

- (void)holdTopViewInScrollView:(UIScrollView*)scrollView lastContentOffset:(NSValue*)lastContentOffset
{
    if (self.topView) {
        float lastPointY = [lastContentOffset CGPointValue].y;
        
//        SQLOGRECT(frame);
//        SQLOGPOINT(scrollView.contentOffset);
        
        if (lastPointY > scrollView.contentOffset.y && scrollView.contentOffset.y < 0) {
            
                // ScrollDirectionDown
                
                CGRect frame = self.topView.frame;
//                SQLOGRECT(frame);
//                SQLOGPOINT(scrollView.contentOffset);
                frame.origin.y = scrollView.contentOffset.y;
            
                self.topView.frame = frame;
            
        } else if (lastPointY < scrollView.contentOffset.y) {
            
            // ScrollDirectionUp
            
        }
    }
}

- (void)resetTopViewInScrollView
{
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.topView.frame = self.initTopViewFrame;
    }];
}
@end

