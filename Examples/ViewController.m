//
//  ViewController.m
//  sqtest
//
//  Created by apple on 13-9-6.
//  Copyright (c) 2013年 boohee. All rights reserved.
//

#import "ViewController.h"
#import "AFJSONRequestOperation.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)initData
{
    [super initData];
    
    NSMutableArray* array = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i <= 10; i ++) {
        NSArray* imageList = @[@"defaultAvatar",@"defaultAvatar",@"defaultAvatar"];
        
        if (i % 2 == 0) {
            imageList = @[@"https://www.google.com.hk/images/srpr/logo4w.png"];
        }
        
        NSMutableArray* images = [[NSMutableArray alloc] init];
        
        for (NSString* imageName in imageList) {
            
            SQCellImage* cellImage = [[SQCellImage alloc] init];
            cellImage.imageURL = imageName;
            cellImage.originalImageURL = imageName;
            cellImage.placeHolderWeight = 200.0;
            cellImage.placeHolderHeight = 80;
            
            [images addObject:cellImage];
            
        }
        
        SQCellModel* model = [[SQCellModel alloc] initWithIndex:i
                                            avatar:nil
                                          nickname:[NSString stringWithFormat:@"name: %d",i]
                                              date:[NSDate date]
                                           content:@"测试测试测试测试测试测试测测试测试测试测试测试测试测测试测试测试测试测试测试测测试测试测试测试测试测试测"
                                            images:images];
        
        [array addObject:model];
    }
    
    [self.cells addObjectsFromArray:array];    
    
    SQUserModel* userModel = [[SQUserModel alloc] initWithIndex:1 avatar:@"Icon-120" nickname:@"square" summary:@".........."];

    self.currentUser = userModel;

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)realodData:(SQPullRefreshDataSource)source_
{
    [super realodData:source_];
    
    NSURL *url = [NSURL URLWithString:@"http://192.168.2.88:3004/api/v1/home_timeline.json?token=1234"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSArray* posts = [JSON valueForKey:@"posts"];
        
        NSDictionary* userDict = [JSON valueForKey:@"user"];        
        
        SQUserModel* userModel = [[SQUserModel alloc] initWithIndex:[[userDict valueForKey:@"id"] integerValue] avatar:[userDict valueForKey:@"avatar_url"] nickname:[userDict valueForKey:@"nickname"] summary:[userDict valueForKey:@"description"]];
        self.currentUser = userModel;
        
        
        NSMutableArray* cellsMore = [[NSMutableArray alloc] init];
        
        for (NSDictionary* dict in posts) {
            
            NSString *dateString = [dict objectForKey:@"created_at"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
            NSDate *dateFromString = [[NSDate alloc] init];
            dateFromString = [dateFormatter dateFromString:dateString];

                        
            SQCellModel* model = [[SQCellModel alloc] initWithIndex:[[dict objectForKey:@"id"] integerValue] avatar:[[dict objectForKey:@"user"] objectForKey:@"avatar_url"] nickname:[[dict objectForKey:@"user"] objectForKey:@"nickname"] date:dateFromString content:[dict objectForKey:@"body"] images:nil videoPath:nil vote:0];
            
            if (source_ == SQPullRefreshDataOfNew) {

                [cellsMore addObject:model];

            } else if (source_ == SQPullRefreshDataOfPast) {
            
                [cellsMore insertObject:model atIndex:0];
                
            }
            
        }
        
        [super doneLoadingData:source_ attachData:cellsMore];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        [super doneLoadingData:source_ attachData:nil];
        
    }];
    [operation start];
}

@end
