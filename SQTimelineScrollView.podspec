Pod::Spec.new do |s|
  s.name     = 'SQTimelineScrollView'
  s.version  = '0.2'
  s.platform = :ios, '5.0'
  s.license  = 'MIT'
  s.summary  = 'Simulate Path scroll view by time line'
  s.homepage = 'https://squarezw@bitbucket.org/squarezw/sqtimelinescrollview'
  s.authors  = 'Square'
  s.source   = { :git => 'https://squarezw@bitbucket.org/squarezw/sqtimelinescrollview.git', :tag => s.version.to_s }
  s.source_files = 'SQTimeline/**/*.{h,m,plist}'
  s.requires_arc = true
  s.framework = 'QuartzCore'
  s.dependency 'SDWebImage'
  s.dependency 'GGFullscreenImageViewController', '1.0.0'
  s.dependency 'NRSimplePlist'
  s.dependency 'NSDate+TimeAgo', '1.0.2'

  s.ios.deployment_target = '4.3'
  s.osx.deployment_target = '10.6'
end
